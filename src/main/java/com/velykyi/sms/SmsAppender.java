package com.velykyi.sms;

import java.io.Serializable;

import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
public final class SmsAppender extends AbstractAppender {
    protected SmsAppender(String name, Filter filter, Layout<? extends Serializable> layout,final boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    public void append(LogEvent logEvent) {
        try {
            ExampleSMS.send(new String(getLayout().toByteArray(logEvent)));
        } catch (Exception e) {
        }
    }

    @PluginFactory
    public static SmsAppender createAppender(@PluginAttribute("name") String name,
                                             @PluginAttribute("Layout") Layout<? extends Serializable> layout,
                                             @PluginAttribute("Filter") final Filter filter,
                                             @PluginAttribute("OtherAttribute") String otherAttribute) {
        if (name == null) {
            LOGGER.error("No name provided for MyCustomAppenderImpl");
            return null;
        }
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new SmsAppender(name, filter, layout, true);
    }
}
