package com.velykyi.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "ACb349f4d7f64d11733ae0be36359cf745";
    public static final String AUTH_TOKEN = "d2c35a8531af036c845477c28fa218a9";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380676766220"),
                new PhoneNumber("+12053410561"), str).create();
    }
}
