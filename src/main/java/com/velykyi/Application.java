package com.velykyi;

import org.apache.logging.log4j.*;

public class Application {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
//        logger1.trace("This is trace message!");
//        logger1.debug("This is debug message!");
//        logger1.info("This is an info message");
//        logger1.warn("This is a warn message");
//        logger1.error("This is error message!");
//        logger1.fatal("This is a fatal message");
        for (int i = 0; i < 100; i++) {
            logger1.trace("This is trace message!");
            logger1.debug("This is debug message!");
            logger1.info("This is an info message");
            logger1.warn("This is a warn message");
            logger1.error("This is error message!");
            logger1.fatal("This is a fatal message");
        }
    }
}
